export const state = () => ({
  footerNav: {},
  contact: {}
})

export const mutations = {
  SET_NAV_FOOTER (state, footerNav) {
    state.footerNav = footerNav
  },
  GET_CONTACT(state, contact) {
    state.contact = contact
  },
  SET_ERROR (state, error) {
    state.footerNav = error
  }
}

export const actions = {
  async fetchMenu ({ commit }, $prismic) {
    try {
      const navigations = (await $prismic.api.query(
        $prismic.predicates.at('document.type', 'navigation')
      )).results

      commit('SET_NAV_FOOTER', navigations[0].data.body[0].items.map((item) => {
        item.slug = item.title.toLowerCase().replace(' ', '-');
        return item;
      }));

      const contact = (await $prismic.api.getSingle('contact')).data;
      commit('GET_CONTACT', contact);

    } catch (e) {
      const error = 'Cannot connect to backend'

      commit('SET_ERROR', e);
    }
  }
}
